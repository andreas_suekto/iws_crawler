package iws_ass2.smartcrawler;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import weka.classifiers.bayes.NaiveBayesUpdateable;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.AbstractFileSaver;
import weka.core.converters.ConverterUtils;
import weka.core.converters.ConverterUtils.DataSource;

public class NaiveBayesUpdatableClassifier {
	private Instances dataSet;
	private NaiveBayesUpdateable nbuClassifier;
	private String arffFilename;
	
	public NaiveBayesUpdatableClassifier (String arffLocation) throws Exception {
		arffFilename = arffLocation;
		
		dataSet = DataSource.read(arffLocation);
		dataSet.setClassIndex(dataSet.numAttributes() - 1);
		
		nbuClassifier = new NaiveBayesUpdateable();
		nbuClassifier.buildClassifier(dataSet);
	}
	
	public ArrayList<String> getFeatureLabels() {
		ArrayList<String> featureLabels = new ArrayList<String>();
		
		int numOfAttribute = dataSet.numAttributes();
		for (int i = 0; i < (numOfAttribute - 1); i++) {
			featureLabels.add(dataSet.attribute(i).name());
		}
		
		return featureLabels;
	}
	
	public String getClassType(Map<String, Double> distributionMap) {
		String classType = "";
		double score = -1.0;
		
		Set<String> keySet = distributionMap.keySet();
		for (String key: keySet) {
			double currentScore = distributionMap.get(key);
			if (score < currentScore) {
				classType = key;
				score = currentScore;
			}
		}
		
		return classType;
	}
	
	public String getClassType(ArrayList<Double> scores) throws Exception {
		Map<String, Double> distributionMap = getClassDistribution(scores);
		return getClassType(distributionMap);
	}
	
	public String getClassType(Instance testData) throws Exception {
		Map<String, Double> distributionMap = getClassDistribution(testData);
		return getClassType(distributionMap);
	}
	
	public Map<String, Double> getClassDistribution(ArrayList<Double> scores) throws Exception {
		// prepare the instance mode
		String dummmyClassType = dataSet.classAttribute().value(0);
		Instance testData = convertToRow(scores, dummmyClassType);
		
		return getClassDistribution(testData);
	}
	
	public Map<String, Double> getClassDistribution(Instance testData) throws Exception {
		Map<String, Double> distribution = new HashMap<String, Double>();
		
		double[] distValues = nbuClassifier.distributionForInstance(testData);
		
		for (int i = 0; i < distValues.length; i++) {
			distribution.put(dataSet.classAttribute().value(i), distValues[i]);
		}
		
		return distribution;
	}
	
	public void updateRow(ArrayList<Double> scores, String classType) throws Exception {
		Instance row = convertToRow(scores, classType);
		nbuClassifier.updateClassifier(row);
		dataSet.add(row);
		updateARFFFile();
	}
	
	private void updateARFFFile() throws IOException {
		AbstractFileSaver fileSaver = ConverterUtils.getSaverForFile(arffFilename);
		try {
			fileSaver.setFile(new File(arffFilename));
			fileSaver.setInstances(dataSet);
			fileSaver.writeBatch();
		} 
		finally {
			if (fileSaver.getWriter() != null)
				fileSaver.getWriter().close();
		}
	}
	
	private Instance convertToRow (ArrayList<Double> scores, String classType) {
		// prepare the values, 
		// the total # of cols is scores size plus the classtype
		double[] values = new double[scores.size() + 1];
		
		// convert the scores
		for (int i = 0; i < scores.size(); i++) {
			values[i] = scores.get(i);
		}
		// finally convert the class
		values[values.length - 1] = dataSet.attribute(dataSet.numAttributes() - 1).indexOfValue(classType);
		
		// pack everything into a row
		Instance row = new DenseInstance(1.0, values);
		row.setDataset(dataSet);
		
		return row;
	}
	
	public static void main(String[] args) throws Exception {
		String arffLoc = "res/model.arff";
		NaiveBayesUpdatableClassifier classifier = new NaiveBayesUpdatableClassifier(arffLoc);
		
		System.out.println("Classification Test");
		Instances dataSet = DataSource.read(arffLoc);
		dataSet.setClassIndex(dataSet.numAttributes() - 1);
		
		Instance data = dataSet.get(3);
		ArrayList<Double> scores = new ArrayList<Double>();
		for (int i = 0; i < (data.numAttributes()-1); i++) {
			scores.add(data.value(i));
		}
		
		String actualClassType = dataSet.classAttribute().value((int) data.value(data.numAttributes()-1));
		Map<String, Double> dist = classifier.getClassDistribution(scores);
		
		System.out.println("Actual class : " + actualClassType);
		System.out.println("Predicted class : " + classifier.getClassType(dist));
		System.out.println("Distribution : " + dist);
		System.out.println("Finish");
		
		System.out.println("Update Row Test");
		classifier.updateRow(scores, actualClassType);
		
		System.out.println("Show Feature Labels Test");
		System.out.println(classifier.getFeatureLabels());
	}
}
