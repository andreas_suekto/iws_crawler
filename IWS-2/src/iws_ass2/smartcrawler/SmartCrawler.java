package iws_ass2.smartcrawler;

import java.util.ArrayList;
import java.util.Scanner;

import iws_ass2.crawler.Crawler;
import iws_ass2.crawler.CrawlerException;
import iws_ass2.crawler.Page;
import iws_ass2.modeller.ARFFGenerator;
import iws_ass2.term.page.HTMLTermGenerator;
import iws_ass2.term.page.PageTermList;
import iws_ass2.util.Configuration;
import iws_ass2.util.FileUtil;

public class SmartCrawler {
	//SmartCrawler always presents found pages to user between
	//fetches - so politeness guaranteed
	private final static int POLITENESS_DEFAULT = 1;
	private final static int NUMPAGES_DEFAULT = 10;
	private final static String USAGE_STRING = "./smartcrawl [-politeness <seconds>] "
			+ "[-mobilepages <pages>] seed_url";
	
	private HTMLTermGenerator termGenerator;
	private NaiveBayesUpdatableClassifier classifier;
	private Crawler crawler;
	private String outdir;
	private int requestedPages;
	//to hold scores for most recently classified page
	ArrayList<Double> scores = new ArrayList<Double>();
	
//CONSTRUCTOR
	public SmartCrawler(int politeness, int numPages, String url) throws CrawlerException, Exception{
		this.requestedPages = numPages;
		
		String stopwordFile;
		String arffFile;
		
		Configuration config = new Configuration(FileUtil.DEF_CONFIG_LOCATION);
		stopwordFile  = config.get(Configuration.FILE_STOP_WORDS);
		arffFile = config.get(Configuration.FILE_MODEL);
		outdir = config.get(Configuration.DIR_SAVED_PAGES);
		
		termGenerator = new HTMLTermGenerator(stopwordFile);
		classifier = new NaiveBayesUpdatableClassifier(arffFile);
		//initialise crawler to return 1 page at a time
		crawler = new Crawler(politeness, 1, url, false, outdir);	
	}
	
//HELPER FUNCTIONS
	/*Inputs one of specified range of characters. Accepts upper or lower case.
	Keeps asking until response is one of the requested range.*/
	private char askForChar(String q, String charList){
	
		@SuppressWarnings("resource")
		Scanner keyboard = new Scanner(System.in);
		String answer;
		char response=' ';
		boolean valid = false;
	
		do{
			System.out.print(q);
			try{
				answer = keyboard.nextLine();
				answer=answer.trim().toUpperCase();
				if (charList.toUpperCase().indexOf(answer)<0) throw new Exception("Please enter one of the following characters: "+charList+"\n");
				response = answer.charAt(0);
				valid = true;
			}catch(IndexOutOfBoundsException oob){
				//null entry - repeat question
			}catch(Exception e){
				System.err.println(e.getMessage());
				e.printStackTrace();
				break;
			}
		}while (!valid);
	
		return response;
	}

	
	private boolean isMobilePage(String filename) throws Exception{
		boolean isMobile = false;
		ArrayList<String> labels = new ArrayList<String>();
		scores.clear();
		String label, classification;
		double frequency;
		
		//generate pagetermlist
		PageTermList ptl = termGenerator.generate(outdir + "/" + filename);
		//get feature labels
		labels = classifier.getFeatureLabels();
		//use ptl and classifier to get scores
		for(int i=0; i<labels.size(); i++){
			label = labels.get(i);
			if (ptl.get(label) == null) {
				frequency = 0;
			}else {
				frequency = ptl.get(label);
			}
			scores.add(frequency/(ptl.getDocumentLength()/100.0));
		}
		//get classification and convert to boolean
		classification = classifier.getClassType(scores);
		isMobile = classification.equals(ARFFGenerator.CLASS_MOBILE) ? true : false;
		
		return isMobile;
	}

	public int presentPages(){
		Page offering;
		int mobilePages=0;
		int fetched;
		String filename, flipClass;
		char response;
		boolean mobile, quit=false;
		
		System.out.println(" WELCOME TO THE SMARTCRAWLER\n"+
						   "=============================");
		System.out.println("You requested " + requestedPages + " mobile pages.");
		
		fetched = crawler.crawlPages();
		while (!quit && fetched>0 && mobilePages<requestedPages){
			offering = crawler.getLastPage();
			filename = offering.makeName(offering.getId());
			try{
				if(isMobilePage(filename)){
					mobile = true;
					flipClass = ARFFGenerator.CLASS_NONMOBILE;
					System.out.println("\nPlease confirm that the following IS a mobile page:");
				}else{
					mobile = false;
					flipClass = ARFFGenerator.CLASS_MOBILE;
					System.out.println("\nPlease confirm that the following is NOT a mobile page:");
				}
			}catch(Exception e){
				//failed to obtain classification - skip page
				fetched = crawler.crawlPages();
				continue;
			}
			System.out.println(offering.getUrl());
			response = askForChar("True or False? T/F - or enter Q to quit: ","TFQ");
			switch(response){
				case 'F':
					mobile = !mobile;
					System.out.println("Thanks for your feedback. Updating classifier...\n");
					try {
						classifier.updateRow(scores, flipClass);
					} catch (Exception e) {
						// update failed. Try to continue
						System.out.println("Update failed.");
					}
					break;
				case 'Q':
					quit = true;
					break;
			}
			if(mobile){
				mobilePages++;
			}
			if(!quit)fetched = crawler.crawlPages();
		}
		return mobilePages;
	}
	
	
	//MAIN METHOD
	public static void main(String[] args){
		//main - called for part 3
		String nextArg, seed="";
		int num = NUMPAGES_DEFAULT, polite = POLITENESS_DEFAULT;
		try{
			for(int a=0;a<args.length;a++){
				nextArg = args[a];
				if(nextArg.equals("-politeness")){
					try{
						polite = Integer.parseInt(args[a+1]);
					}catch(NumberFormatException e){
						System.out.println("Illegal argument.");
						System.out.println("Usage:" + USAGE_STRING);
						System.exit(1);
					}
					a++;
				}else if(nextArg.equals("-mobilepages")){
					try{
						num = Integer.parseInt(args[a+1]);
					}catch(NumberFormatException e){
						System.out.println("Illegal argument.");
						System.out.println("Usage:" + USAGE_STRING);
						System.exit(1);
					}
					a++;
				}else if(nextArg.startsWith("-")){
					System.out.println("Unknown argument: " + nextArg);
					System.out.println("Usage:" + USAGE_STRING);
					System.exit(1);
				}else{
					seed = nextArg;
				}
			}
		
		// initialise smartCrawler, fetch pages and present to user.
			SmartCrawler sc = new SmartCrawler(polite, num, seed);
			sc.presentPages();
			System.out.println("Thank you for using SmartCrawler!");
		}catch(CrawlerException e){
			System.err.println(e);
			System.exit(1);
		}catch(Exception e){
			System.err.println("Terminating program...");
			System.exit(1);
		}
	}
}
