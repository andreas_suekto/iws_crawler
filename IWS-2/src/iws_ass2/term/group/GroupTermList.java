package iws_ass2.term.group;

import iws_ass2.datastructure.TermList;

public class GroupTermList extends TermList<Double>{
	private int numberOfDocuments;
	
	public GroupTermList () {
		super();
		numberOfDocuments = 0;
	}
	
	public int getDocumentLength () {
		return numberOfDocuments;
	}
	
	/**
	 * Add a term in a list.
	 * Increment the frequency of a term if exist beforehand.
	 * @param term
	 */
	public void put (String term, Double pValue) {
		Double gValue = termList.get(term);
		if (gValue != null) {
			// Existing term
			gValue += pValue;
			termList.put(term, gValue);
		}
		else {
			// Firstly found term, initialize the data
			termList.put(term, pValue);
		}
		numberOfDocuments++;
	}

}
