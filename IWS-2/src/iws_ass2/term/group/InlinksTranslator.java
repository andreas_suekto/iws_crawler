package iws_ass2.term.group;

import iws_ass2.crawler.Page;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class InlinksTranslator {
	
	private Map<String, Page> inlinksMap;
	
	public Page get(String filename) {
		return inlinksMap.get(filename);
	}
	
	public InlinksTranslator(String inlinksFileLocation) throws IOException {
		inlinksMap = new HashMap<String, Page>();
		generateInlinksMap(inlinksFileLocation);
	}
	
	/**
	 * 
	 * @return
	 * a map with the key of the mobile file id/name, and
	 * the value of 
	 * @throws IOException 
	 */
	private void generateInlinksMap (String inlinksFileLocation) throws IOException {
		String inlinksStr = getInlinksString(inlinksFileLocation); 
		String[] lines = inlinksStr.split("\n");
		HashMap<String, Integer> inLinks;
		
		for (String line : lines) {
			String fileName = ""; 
			String url = "";
			inLinks = new HashMap<String, Integer>();
			
			String[] splittedByComa = line.split(",");
			if (splittedByComa.length > 2) {
				String firstRegex = "^(\\d{4}),(.+?),(.*)";
				fileName = line.replaceAll(firstRegex, "$1");
				url = line.replaceAll(firstRegex, "$2");
				
				String inlinkListStr = line.replaceAll(firstRegex, "$3");
				inlinkListStr = inlinkListStr.replaceAll(",", " ");
				String[] inlinksWithValue = inlinkListStr.split(";");
				for (String inlinkVal : inlinksWithValue) {
					if (inlinkVal.trim().isEmpty())
						break;
					String[] splittedInlinkVal = inlinkVal.split("\":");
					String anchorText = splittedInlinkVal[0];
					anchorText = anchorText.replaceAll("\"", "");
					int num = Integer.parseInt(splittedInlinkVal[1]);
					inLinks.put(anchorText, num);
				}
			}
			else if (splittedByComa.length == 2) {
				fileName = splittedByComa[0];
				url = splittedByComa[1];
			}
			else {
				break;
			}
			
			Page page = new Page(url);
			page.getLinks().putAll(inLinks);
			
			inlinksMap.put(fileName, page);
			//System.out.println(fileName+"\n\t"+page.getUrl()+"\n\t"+page.getLinks());
		}
	}
	
	private String getInlinksString (String inlinksFileLocation) throws IOException {
		StringBuilder inlinksStr = new StringBuilder();
		InputStreamReader reader = null;
		try {
		reader = new InputStreamReader(
										new BufferedInputStream(
											new FileInputStream(inlinksFileLocation)));
		
			// Convert file into string
			int c;
			while ((c = reader.read()) != -1) {
				inlinksStr.append((char)c);
			}
		}
		finally {
			if (reader != null)
				reader.close();
		}
		return inlinksStr.toString();
	}
	
	public static void main(String[] args) throws IOException {
		new InlinksTranslator("res/inlinks.txt");
	}

}
