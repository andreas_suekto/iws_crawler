package iws_ass2.term.group;

import iws_ass2.crawler.Page;
import iws_ass2.term.page.HTMLTermGenerator;
import iws_ass2.term.page.PageTermList;
import iws_ass2.term.tools.WordFilter;
import iws_ass2.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

public class GroupTermGenerator {
	private final static double INLINK_WEIGHT = 1;
	private HTMLTermGenerator pageTermGenerator;
	private InlinksTranslator inlinksTranslator;
	private WordFilter wordFilter;
	
	public GroupTermGenerator (String stopWordsLocation, String inlinksFileLocation) 
			throws IOException 
	{
		pageTermGenerator = new HTMLTermGenerator(stopWordsLocation);
		wordFilter = new WordFilter(stopWordsLocation);
		if (inlinksFileLocation != null)
			inlinksTranslator = new InlinksTranslator(inlinksFileLocation);
		else 
			inlinksTranslator = null;
	}
	
	
	/**
	 * Calculates the group term values for the set
	 * of pages described by the offline location.
	 * 
	 * Should be called before a call to getGroupTermList()
	 * and/or getNumberOfDocuments.
	 * 
	 * @param offlineFileLocations
	 * @throws IOException
	 */
	public GroupTermList calculate (Set<String> offlineFileLocations
										, String saveDirectoryLocation
										, boolean isMobileRelated) 
			throws IOException {
		GroupTermList groupTermList = new GroupTermList();
		
		for (String pageLocation : offlineFileLocations){
			//System.out.println(pageLocation);
			// retrieve term-list for a page
			PageTermList pTermList = pageTermGenerator.generate(pageLocation);
			
			// Put each term data into group-term-list
			Set<String> terms = pTermList.getKeySet();
			for (String term : terms) {
				// calculate term value = ratio between value of term and document's length
				Integer pFrequency = pTermList.get(term);
				Double pTermValue = ((double)pFrequency) / (pTermList.getDocumentLength()/100.0);
				// put it into the group list.
				groupTermList.put(term, pTermValue);
			}
			
			// Process Inlinks if given
			if (isMobileRelated && inlinksTranslator != null) {
				File pageFile = new File(pageLocation);
				String pageFileName = pageFile.getName();
				Page page = inlinksTranslator.get(pageFileName);
				if (page != null) {
					HashMap<String, Integer> inlinks = page.getLinks();
					Set<String> linkSet = inlinks.keySet();
					for (String link : linkSet) {
						Double value 
							= (inlinks.get(link) * INLINK_WEIGHT)
											/ (pTermList.getDocumentLength() / 10);
						
						StringTokenizer tokenizer = new StringTokenizer(link);
						while (tokenizer.hasMoreTokens()) {
							String filteredToken = wordFilter.filter(tokenizer.nextToken());
							if (filteredToken != null) {
								groupTermList.put(filteredToken, value);
							}
						}
					}
				}
			}
			
			if (saveDirectoryLocation != null) {
				File saveDir = new File(saveDirectoryLocation);
				if (saveDir.exists()==false) {
					saveDir.mkdir();
				}
				File pageFile = new File(pageLocation);
				//System.out.println(saveDir.getAbsolutePath() + File.separator + pageFile.getName() + ".terms");
				pTermList.saveToFile(saveDir.getAbsolutePath() + File.separator + pageFile.getName() 
										+ FileUtil.getTermsFileExtension());
			}
			
			pTermList.clear();
		}
		
		return groupTermList;
	}
	
	public static void main (String[] args) throws IOException {
		GroupTermGenerator gTermGenerator = new GroupTermGenerator("res/english.stop.txt", null);
		Set<String> files = new HashSet<String>();
		files.add("res/mobile-page/01");
		files.add("res/mobile-page/02");
		
		GroupTermList gTermList = gTermGenerator.calculate(files, "res/mobile-page/terms", true);
		System.out.println("Document Length : " + gTermList.getDocumentLength());
		for (String term : gTermList.getKeySet()) {
			System.out.println(term + " : " + gTermList.get(term));
		}
	}
}
