package iws_ass2.term.page;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class HTMLParser {
	private final static String LABEL_IGNORE = "#IGNORE";
	private final static String LABEL_CARE = "#CARE";
	
	public String parse (String fileLocation) throws IOException {
		return parse(new FileInputStream(fileLocation));
	}
	
	public String parse (InputStream inputStream) throws IOException {
		StringBuilder originalHTML = new StringBuilder();
		
		// can not use Scanner, somehow when the file is too big, not everything being read.
		// Make sure to chain with buffer for more efficient IO processing.
		InputStreamReader reader = null;
		String filtered = null;
		try {
			reader = new InputStreamReader(new BufferedInputStream(inputStream));
			
			// Convert file into string
			int c;
			while ((c = reader.read()) != -1) {
				originalHTML.append((char)c);
			}
			
			filtered = originalHTML.toString();
			// Filter out javascripts and css
			filtered = filterTagAndContent(filtered, "script");
			filtered = filterTagAndContent(filtered, "style");
			
			// In case of no script they usually provide &lt; and &gt;
			filtered = filtered.replaceAll("&lt;","<");
			filtered = filtered.replaceAll("&gt;",">");
			// Filter out all tags
			filtered = filterAllTag(filtered);
			
			// Remove all special XML characters
			String[] specialWordToRemove = new String[]{"&amp;", "&nbsp;"};
			for (String specWord : specialWordToRemove) {
				filtered = filtered.replaceAll(specWord," ");
			}
			
			// Remove every non-character-digit word
			filtered = filtered.replaceAll("[^\\w\\n]"," ");
			
			// Compacting multiple breaklines and spaces
			filtered = filtered.replaceAll("  +"," ");
			filtered = filtered.replaceAll(" *\\n *","<");
			filtered = filtered.replaceAll("<+","\n");
		}
		finally {
			if (reader != null)
				reader.close();
		}
		return filtered;
	}
	
	/**
	 * Delete all tags
	 * @param original
	 * @return
	 */
	private String filterAllTag(String original) {
		String filtered = original;
		filtered = filtered.replaceAll("<.*?>"," ");
		
		return filtered;
	}
	
	/**
	 * Delete the tag and the content between it
	 * @param original
	 * @param tagName
	 * @return
	 */
	private String filterTagAndContent(String original, String tagName) {
		String filtered = original;
		filtered = filtered.replaceAll("<"+tagName+".*?>","<"+tagName+">");
		filtered = filtered.replaceAll("</"+tagName+".*?>", "</"+tagName+">");
		filtered = filtered.replaceAll("<"+tagName+"></"+tagName+">", "");
		filtered = filtered.replaceAll("<"+tagName+">", "\n"+LABEL_IGNORE+"\n");
		filtered = filtered.replaceAll("</"+tagName+">", "\n"+LABEL_CARE+"\n");
		
		StringBuilder filteredBuilder = new StringBuilder();
		String[] filteredLineArray = filtered.split("\n");
		boolean care = true;
		for (int i = 0; i < filteredLineArray.length; i++) {
			String line = filteredLineArray[i].trim();
			if (line.equals(LABEL_IGNORE)) {
				care = false;
			}
			else if (line.equals(LABEL_CARE)) {
				care = true;
			}
			else if (care) {
				if (line.length() > 0) {
					filteredBuilder.append(line+"\n");
				}
			}
		}
		filtered = filteredBuilder.toString();
		
		return filtered;
	}
	
	public static void main(String[] args) throws IOException {
		HTMLParser parser = new HTMLParser();
		System.out.println(parser.parse("res/mobile-page/02"));
		
	}

}
