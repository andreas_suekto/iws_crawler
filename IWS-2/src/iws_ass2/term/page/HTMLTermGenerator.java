package iws_ass2.term.page;

import iws_ass2.term.tools.WordFilter;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.StringTokenizer;

public class HTMLTermGenerator {
	private String pageText;
	private WordFilter wordFilter;
	
	
	public HTMLTermGenerator(String stopWordsLocation) throws FileNotFoundException {
		// Initialize all member fields
		wordFilter = new WordFilter(stopWordsLocation);
	}
	
	public PageTermList generate (String offlinePageLocation) throws IOException {
		pageText = getText(offlinePageLocation);
		return generate();
	}
	
	public PageTermList generate (URL pageLocation) throws IOException {
		pageText = getText(pageLocation);
		return generate();
	}
	
	private PageTermList generate() {
		PageTermList termList = new PageTermList();
		
		StringTokenizer tokenizer = new StringTokenizer(pageText);
		while (tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken().trim();
			String filteredToken = wordFilter.filter(token);
			if (filteredToken != null) {
				termList.put(filteredToken);
			}
		}
		
		return termList;
	}
	
	
	/**
	 * Get the text of an online HTML Page
	 * @param htmlURL
	 * @return
	 * @throws IOException
	 */
	private String getText (URL htmlURL) throws IOException {
		return getText(htmlURL.openStream());
	}
	
	/**
	 * Get the text of an offline HTML Page
	 * @param offlinePageLocation
	 * @return
	 * @throws IOException
	 */
	private String getText (String offlinePageLocation) throws IOException {
		return getText(new FileInputStream(offlinePageLocation));
	}
	
	/**
	 * General Text getter of an HTML
	 * @param htmlStream
	 * @return
	 * @throws IOException
	 */
	private String getText (InputStream htmlStream) throws IOException {
		HTMLParser parser = new HTMLParser();
		
		return parser.parse(htmlStream);
	}
	
	
	public static void main(String[] args) {
		try {
			// saving-test
			HTMLTermGenerator termGenerator 
					= new HTMLTermGenerator("res/english.stop.txt");
			PageTermList termList = termGenerator.generate("res/mobile-page/02");
			termList.saveToFile("res/mobile-page/01.terms");
			System.out.println("Finish saving.");
			
//			//reading-test
//			PageTermList termList = new PageTermList("res/mobile-page/01.terms");
//			Set<String> terms = termList.getKeySet();
//			System.out.println("Document Length : " + termList.getDocumentLength());
//			for (String term : terms) {
//				System.out.println(term + " : " + termList.get(term));
//			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
