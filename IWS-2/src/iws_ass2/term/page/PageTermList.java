package iws_ass2.term.page;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Scanner;
import java.util.Set;

import iws_ass2.datastructure.TermList;

public class PageTermList extends TermList<Integer> {
	private int documentLength;
	private final static String NEWLINE = "\n";
	
	public PageTermList () {
		super();
		documentLength = 0;
	}
	
	public PageTermList (String fileLocation) throws FileNotFoundException {
		Scanner reader = null;
		
		try {
			reader = new Scanner(new File(fileLocation));
			
			if (reader.hasNextLine()) {
				documentLength = Integer.parseInt(reader.nextLine());
			}
			while(reader.hasNextLine()) {
				String row = reader.nextLine();
				String[] dataInRow = row.split(":");
				
				String term = dataInRow[0].trim();
				Integer frequency = Integer.parseInt(dataInRow[1]);
				
				termList.put(term, frequency);
			}
		}
		finally {
			if (reader != null)
				reader.close();
		}
	}
	
	public int getDocumentLength () {
		return documentLength;
	}
	
	/**
	 * Add a term in a list.
	 * Increment the frequency of a term if exist beforehand.
	 * @param term
	 */
	public void put (String term) {
		Integer frequency = termList.get(term);
		if (frequency != null) {
			// Existing term
			frequency++;
			termList.put(term, frequency);
		}
		else {
			// Firstly found term, initialize the data
			termList.put(term, 1);
		}
		documentLength++;
	}
	
	public void saveToFile(String targetLocation) throws IOException {
		OutputStreamWriter writer = null;
		try {
			writer = new OutputStreamWriter(new FileOutputStream(new File(targetLocation)));
		
			// First line is document length
			writer.write(getDocumentLength()+"");
					
			Set<String> terms = getKeySet();
			for (String term : terms) {
				// Print  term:frequency 
				writer.write(NEWLINE + term + ":" + get(term));
			}
		}
		finally {
			writer.close();
		}
	}
}
