package iws_ass2.term.tools;

/**
 * String to be used by Porter stemmer algorithm.
 * Some word statistic calculation related to Porter algorithm are provided.
 */
public class PorterString implements CharSequence {
    // List of vowel characters, excluding special case of 'y'.
    private static char[] vowelChars = new char[] {'a', 'i', 'u', 'e', 'o'};
    
    private StringBuilder builder;
    
    public PorterString(CharSequence charSequence) {
        builder = new StringBuilder(charSequence);
    }
    
    @Override
    public char charAt(int i) {
        return builder.charAt(i);
    }

    @Override
    public int length() {
        return builder.length();
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        return builder.subSequence(start, end);
    }

    @Override
    public String toString() {
        return builder.toString();
    }
    
    public void deleteFrom(int i) {
        builder.delete(i, builder.length());
    }
    
    public void replaceFrom(int i, String str) {
        builder.replace(i, builder.length(), str);
    }
    
    public void append(char ch) {
        builder.append(ch);
    }
    
    public void append(CharSequence ch) {
        builder.append(ch);
    }
    
    /**
     * Check if this string end with the given suffix.
     */
    public boolean hasSuffix(CharSequence suffix, int length) {
        if (suffix.length() > length) return false;
        
        int suffixLength = suffix.length();        
        for (int i = 1; i <= suffixLength; i++) {
            char s = suffix.charAt(suffixLength - i);
            char b = builder.charAt(length - i);
            
            if (s != b) return false;
        }
        
        return true;
    }
    
    public boolean hasSuffix(CharSequence suffix) {
        return hasSuffix(suffix, builder.length());
    }
    
    /**
     * Check if this string end with a double consonant.
     */
    public boolean hasDoubleConsonantEnd() {
        int l = builder.length();
        if (l < 2) return false;
        
        if (! isConsonant(l - 1)) return false;
        return builder.charAt(l - 1) == builder.charAt(l - 2);
    }
    
    /**
     * Check if this string end with CVC (consonant, vowel, consonant) 
     * and the second consonant is not W, X, or Y.
     */
    public boolean hasCvcEnd(int length) {
        if (length < 3) return false;

        if (! (isConsonant(length - 3) && ! isConsonant(length - 2) && isConsonant(length - 1))) 
            return false;
        
        char[] endChars = new char[] { 'w', 'x', 'y' };
        char c3 = builder.charAt(length - 1);
        
        for (int i = 0; i < endChars.length; i++)
            if (c3 == endChars[i]) return false;
        
        return true;
    }
    
    public boolean hasCvcEnd() {
        return hasCvcEnd(builder.length());
    }
    
    /**
     * Check if the given word has a vowel.
     */
    public boolean hasVowel(int length) {
        for (int i = 0; i < length; i++)
            if (! isConsonant(i)) return true;
        
        return false;
    }
    
    public boolean hasVowel() {
        return hasVowel(builder.length());
    }
    
    /**
     * Check if the character at the specified position is a consonant.
     */
    public boolean isConsonant(int position) {
        char c = builder.charAt(position);
        
        for (int i = 0; i < vowelChars.length; i++)
            if (vowelChars[i] == c) return false;
        
        if (c == 'y' && position > 0)
            if (isConsonant(position - 1)) return false; 
        
        return true;
    }
    
    /**
     * The m() function measures the number of consonant sequences in this string.
     * Please check the paper reference for more elaborate explanation about m().
     * 
     * Example:
     * <c><v>       gives 0
     * <c>vc<v>     gives 1
     * <c>vcvc<v>   gives 2
     * <c>vcvcvc<v> gives 3
     */
    public int countM(int length) {
        int m = 0; // Number of consonant sequences. 
        int i = 0; // Index counter
        
        // Skip <c> until the first v is met.
        while (i < length && isConsonant(i)) i++;
        if (i == length) return m;
        
        while (i < length) {
            
            // Skip v until the next c is met.
            while (i < length && ! isConsonant(i)) i++;
            if (i == length) return m;
            
            // Skip c until the next v is met.
            while (i < length && isConsonant(i)) i++;
            m++; // A "vc" combination found, increment m.
        }
        
        return m;
    }
    
    public int countM() {
        return countM(builder.length());
    }
}
