package iws_ass2.term.tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class WordFilter {

	private PorterStemmer stemmer;
	private Set<String> stemmedStopWords;
	
	public WordFilter (String stopWordsLocation) throws FileNotFoundException {
		stemmer = new PorterStemmer();
		// Install stemmed stopWords
		stemmedStopWords = getStopWords(stopWordsLocation);
	}
	
	/**
	 * 
	 * @param word
	 * @return
	 * the filtered word or null if it is stopped or become empty
	 */
	public String filter (String word) {
		String filteredWord = null;
		word = normaliseToken(word).trim();
		
		// Only consider those who are not whitespace
		if (word.length() > 0) {
			// Stem the word
			String stemmedWord = stemmer.stemWord(word);
			// Only add non-stop-words
			if (stemmedStopWords.contains(stemmedWord) == false) {
				filteredWord = stemmedWord;
			}
		}
		
		return filteredWord;
	}
	
	private Set<String> getStopWords(String stopWordsLocation) throws FileNotFoundException {
		Set<String> stemmedStopWords = new HashSet<String>();
		
		Scanner stopWordsFile = null;
		try {
			stopWordsFile = new Scanner(new File(stopWordsLocation));
			while(stopWordsFile.hasNextLine()) {
				String stopWord = stopWordsFile.nextLine().trim();
				if (stopWord.length() > 0) {
					stopWord = stemmer.stemWord(stopWord);
					stemmedStopWords.add(stopWord);
				}
			}
		}
		finally {
			if (stopWordsFile != null)
				stopWordsFile.close();
		}
		return stemmedStopWords;
	}
	
	/**
	 * Token normalisation (lower-casing, removal of non-letter words)
	 */
	private String normaliseToken(String token) {
	    StringBuilder builder = new StringBuilder(token);
	    
	    for (int i = 0; i < builder.length(); i++) {
            char ch = builder.charAt(i);
            
            if (Character.isUpperCase(ch))
                builder.setCharAt(i, Character.toLowerCase(ch));
            
            if (! Character.isLetter(ch)) {
                builder.deleteCharAt(i);
                i--;
            }
        }
	    
	    return builder.toString();
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
