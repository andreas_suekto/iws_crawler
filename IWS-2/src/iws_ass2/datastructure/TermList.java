package iws_ass2.datastructure;

import java.util.HashMap;
import java.util.Set;

public abstract class TermList <V> {

	protected HashMap<String, V> termList;
	
	public TermList () {
		termList = new HashMap<String, V>();
	}
	
	public Set<String> getKeySet() {
		return termList.keySet();
	}
	
	/**
	 * Get the frequency for a certain term
	 * @param term
	 * @return
	 * the frequency, or null if term not found.
	 */
	public V get (String term) {
		return termList.get(term);
	}
	
	public void clear () {
		termList.clear();
	}
}
