package iws_ass2.datastructure;

public class TermScore implements Comparable<TermScore>
{
	private String term;
	private double score;
	
	/**
	 * Data structure to be used in Heap
	 * 
	 * @param term
	 * document identifier
	 * @param score
	 * document score from similarity function
	 */
	public TermScore (String term, double score)
	{
		this.setTerm(term);
		this.setScore(score);
	}
	
	@Override
	public int compareTo(TermScore o) 
	{
		// Compare based on the score value
		if (score > o.score)
			return 1;
		else if (score < o.score)
			return -1;
		else
			return 0;
	}

	public String getTerm() 
	{
		return term;
	}

	public void setTerm(String term) 
	{
		this.term = term;
	}

	public double getScore() 
	{
		return score;
	}

	public void setScore(double score) 
	{
		this.score = score;
	}

	@Override
	public String toString ()
	{
		return "<"+ term + ":"+ score +">";
	}
}
