package iws_ass2.modeller;

import iws_ass2.term.page.PageTermList;
import iws_ass2.util.Configuration;
import iws_ass2.util.FileUtil;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ARFFGenerator {
	private final static String NEW_LINE = "\n";
	private final static String SPACE = " ";
	private final static String COMMA = ",";
	
	private final static String RELATION = "@RELATION pageClassification";
	private final static String PREFIX_ATTRIBUTE = "@ATTRIBUTE";
	private final static String PREFIX_DATA = "@DATA";
	private final static String TYPE_REAL = "REAL";
	public final static String CLASS_MOBILE = "mobile";
	public final static String CLASS_NONMOBILE = "nonmobile";
	private final static String TYPE_CLASS = "{"+CLASS_MOBILE+","+CLASS_NONMOBILE+"}";
	private final static String CLASS_TITLE = "class";
	
	private FeatureSetGenerator featureSetGenerator;
	private List<String> features;
	
	public ARFFGenerator (FeatureSetGenerator featureSetGenerator) throws IOException {
		features = new ArrayList<String>();
		train(featureSetGenerator);
	}
	
	/**
	 * @return
	 * a copy set of the features for ARFF.
	 */
	public Set<String> getFeatureSet() {
		Set<String> featureSet = new HashSet<String>();
		featureSet.addAll(features);
		
		return featureSet;
	}
	
	/**
	 * To generate a fresh features from a featureGenerator
	 * @param featureGenerator
	 * @return
	 * Meant for chaining calls.
	 * @throws IOException
	 */
	public ARFFGenerator train(FeatureSetGenerator featureGenerator) throws IOException {
		features.clear();
		featureSetGenerator = featureGenerator;
		features.addAll(featureSetGenerator.generateFeatureSet());
		
		return this;
	}
	
	public void generate (String targetLocation, String mobileTermsDir, String nonMobileTermsDir) 
			throws IOException 
	{
		StringBuilder arffBuilder = new StringBuilder();
		
		// Header part of ARFF
		installHeader(arffBuilder);
		// DATA part of ARFF
		installBody(arffBuilder, mobileTermsDir, nonMobileTermsDir);
		
		// Generate ARFF File
		OutputStreamWriter writer = null;
		try {
			 writer = new OutputStreamWriter(new BufferedOutputStream(
												new FileOutputStream(targetLocation)));
			writer.write(arffBuilder.toString());
		}
		finally {
			if (writer != null)
				writer.close();
		}
	}

	private void installHeader (StringBuilder arffBuilder) {
		// Relation title
		arffBuilder.append(RELATION + NEW_LINE);
		arffBuilder.append(NEW_LINE);
		
		// Attributes
		for (int i = 0; i < features.size(); i++) {
			arffBuilder.append(PREFIX_ATTRIBUTE + SPACE + features.get(i) + SPACE + TYPE_REAL);
			arffBuilder.append(NEW_LINE);
		}
		// Class Attribute
		arffBuilder.append(PREFIX_ATTRIBUTE + SPACE + CLASS_TITLE + SPACE + TYPE_CLASS + NEW_LINE);
		arffBuilder.append(NEW_LINE);		
	}
	
	private void installBody (StringBuilder arffBuilder, String mobileTermsDir, String nonMobileTermsDir) 
			throws FileNotFoundException 
	{
		// Start writing data
		arffBuilder.append(PREFIX_DATA + NEW_LINE);
		
		// Get file set name
		Set<String> mobileTermsFileNameSet = FileUtil.getFileNameSet(mobileTermsDir);
		Set<String> nonMobileTermsFileNameSet = FileUtil.getFileNameSet(nonMobileTermsDir);
		
		// Generate the mobile row data
		arffBuilder.append(generateAllRowFrom(mobileTermsFileNameSet, CLASS_MOBILE));
		// Generate the nonmobile row data
		arffBuilder.append(generateAllRowFrom(nonMobileTermsFileNameSet, CLASS_NONMOBILE));
	}
	
	
	private String generateAllRowFrom (Set<String> termsFileNameSet, String className) 
			throws FileNotFoundException 
	{
		StringBuilder allRowBuilder = new StringBuilder();
		
		for (String termsFileName : termsFileNameSet) {
			//System.out.println(termsFileName);
			PageTermList pageTermList = new PageTermList(termsFileName);
			allRowBuilder.append(generateRowDataFor(pageTermList, className));
			allRowBuilder.append(NEW_LINE);
		}
		
		return allRowBuilder.toString();
	}
	
	private String generateRowDataFor (PageTermList pageTermList, String className) {
		StringBuilder rowBuilder = new StringBuilder();
		
		for (int i = 0; i < features.size(); i++) {
			double score = 0;
			Integer frequency = pageTermList.get(features.get(i));
			if (frequency != null) {
				score = (double)frequency / (pageTermList.getDocumentLength()/100.0);
			}
			rowBuilder.append(FileUtil.getFormattedScore(score) + COMMA);
		}
		rowBuilder.append(className);
		
		return rowBuilder.toString();
	}
	
	public static void main(String[] args) {
		try {
			// Retrieve configuration
			Configuration config = new Configuration(FileUtil.DEF_CONFIG_LOCATION);
			String mobileDir = config.get(Configuration.DIR_MOBILE);
			String mobileTermsDir = config.get(Configuration.DIR_MOBILE_TERMS);
			String nonmobileDir = config.get(Configuration.DIR_NON_MOBILE);
			String nonmobileTermsDir = config.get(Configuration.DIR_NON_MOBILE_TERMS);
			String stopwordsFile = config.get(Configuration.FILE_STOP_WORDS);
			String inlinksFile = config.get(Configuration.FILE_INLINKS);
			String modelFile = config.get(Configuration.FILE_MODEL);
			int numTopRanksMobile = Integer.parseInt(config.get(Configuration.NUM_MOBILE_FEATURE));
			int numTopRanksNonmobile = Integer.parseInt(config.get(Configuration.NUM_NONMOBILE_FEATURE));
			
			System.out.println("Start generating ARFF . . .");
			FeatureSetGenerator featureGenerator 
					= new FeatureSetGenerator.Builder()
												.setDirMobile(mobileDir)
												.setDirMobileTerms(mobileTermsDir)
												.setDirNonMobile(nonmobileDir)
												.setDirNonMobileTerms(nonmobileTermsDir)
												.setFileStopWords(stopwordsFile)
												.setFileInlinks(inlinksFile)
												.setNumOfTopRanksMobile(numTopRanksMobile)
												.setNumOfTopRanksNonMobile(numTopRanksNonmobile)
												.create();
			
			ARFFGenerator arffGenerator = new ARFFGenerator(featureGenerator);
			arffGenerator.generate(modelFile, mobileTermsDir, nonmobileTermsDir);
			
			System.out.println("Finish Generating ARFF");
		}
		catch (Exception e) {
			System.err.println("Error can not start ARFF Generator.");
			e.printStackTrace();
		}
		
	}

}
