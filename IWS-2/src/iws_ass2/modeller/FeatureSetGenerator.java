package iws_ass2.modeller;

import iws_ass2.datastructure.MinHeap;
import iws_ass2.datastructure.TermScore;
import iws_ass2.term.group.GroupTermGenerator;
import iws_ass2.term.group.GroupTermList;
import iws_ass2.util.FileUtil;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class FeatureSetGenerator {
	
	private String dirMobile = null;
	private String dirNonMobile = null;
	private String dirMobileTerms = null;
	private String dirNonMobileTerms = null;
	private String fileStopWords = null;
	
	private int capMobileHeap;
	private int capNonMobileHeap;
	
	private MinHeap mobileHeap;
	private MinHeap nonMobileHeap;
	private GroupTermGenerator gTermGenerator;
	
	/**
	 * Gateway for builder to start creating the generator
	 * user shall instantiate this using Builder to get
	 * a valid non-confusing parameters.
	 *  
	 * @param fStopWords
	 * @throws IOException 
	 */
	private FeatureSetGenerator (String fInlinks, String fStopWords
									, int cMobileHeap, int cNonMobileHeap) 
			throws IOException 
	{
		capMobileHeap = cMobileHeap;
		capNonMobileHeap = cNonMobileHeap;
		fileStopWords = fStopWords;
		
		mobileHeap = new MinHeap(capMobileHeap);
		nonMobileHeap = new MinHeap(capNonMobileHeap);
		gTermGenerator = new GroupTermGenerator(fileStopWords, fInlinks);
	}
	
	/**
	 * Builder class is meant for clear instantiation
	 * of a FeatureGeneratorSet class. Instead of a chain of String argument,
	 * a chain calls of set function definitely will prevent wrong setting
	 * of the String arguments (such as inversion etc.)
	 */
	public static class Builder {
		private final static int DEF_NUM_TOP_MOBILE_HEAP = 10;
		private final static int DEF_NUM_TOP_NON_MOBILE_HEAP = 10;
		
		private String dirMobile = null;
		private String dirNonMobile = null;
		private String dirMobileTerms = null;
		private String dirNonMobileTerms = null;
		private String fileStopWords = null;
		private String fileInlinks = null;
		
		private int numOfTopRanksMobile = DEF_NUM_TOP_MOBILE_HEAP;
		private int numOfTopRanksNonMobile = DEF_NUM_TOP_NON_MOBILE_HEAP;
		
		/**
		 * A valid creation requires user to set at least the
		 * dirMobile, dirNonMobile, diMobileTerms, and dirNonMobileTerms,
		 * and fileStopWords.
		 * 
		 * Default implementation for numOfTopRanks are 10.
		 * 
		 * @return
		 * @throws IOException 
		 * @throws IllegalArgumentException
		 * in case there is an obligated field not being set.
		 */
		public FeatureSetGenerator create() throws IOException {
			if (dirMobile == null || dirNonMobile == null || dirMobileTerms == null
					|| dirNonMobileTerms == null || fileStopWords == null) {
				throw new IllegalArgumentException("Must set the directory for mobile," +
						"non-mobile, directory for mobile, non-mobile terms, and the" +
						"stopwords file location. Optionally can override the default capacity" +
						"for mobile, and non-mobile terms number to be chosen in feature set" +
						"(named as capMobileHeap, and capNonMobileHeap)");
			}
			
			FeatureSetGenerator generator 
									= new FeatureSetGenerator(fileInlinks
																, fileStopWords
																, numOfTopRanksMobile
																, numOfTopRanksNonMobile);
			
			generator.capMobileHeap = numOfTopRanksMobile;
			generator.capNonMobileHeap = numOfTopRanksNonMobile;
			generator.dirMobile = dirMobile;
			generator.dirNonMobile = dirNonMobile;
			generator.dirMobileTerms = dirMobileTerms;
			generator.dirNonMobileTerms = dirNonMobileTerms;
		
			return generator;
		}
		
		public Builder setFileInlinks(String fileInlinks) {
			this.fileInlinks = fileInlinks;
			return this;
		}

		public Builder setDirMobile(String dirMobile) {
			this.dirMobile = dirMobile;
			return this;
		}
		public Builder setDirNonMobile(String dirNonMobile) {
			this.dirNonMobile = dirNonMobile;
			return this;
		}
		public Builder setDirMobileTerms(String dirMobileTerms) {
			this.dirMobileTerms = dirMobileTerms;
			return this;
		}
		public Builder setDirNonMobileTerms(String dirNonMobileTerms) {
			this.dirNonMobileTerms = dirNonMobileTerms;
			return this;
		}
		public Builder setFileStopWords(String fileStopWords) {
			this.fileStopWords = fileStopWords;
			return this;
		}
		public Builder setNumOfTopRanksMobile(int numOfTopRanksMobile) {
			this.numOfTopRanksMobile = numOfTopRanksMobile;
			return this;
		}
		public Builder setNumOfTopRanksNonMobile(int numOfTopRankNonMobile) {
			this.numOfTopRanksNonMobile = numOfTopRankNonMobile;
			return this;
		}
	}
	
	public Set<String> generateFeatureSet() throws IOException {
		Set<String> featureSet = new HashSet<String>();
		
		// Get the file names
		Set<String> mobileFileNameSet = FileUtil.getFileNameSet(dirMobile);
		Set<String> nonMobileFileNameSet = FileUtil.getFileNameSet(dirNonMobile);
		
		// Calculate the term list for each group
		GroupTermList mobileTermList 
				= gTermGenerator.calculate(mobileFileNameSet, dirMobileTerms, true);
		GroupTermList nonMobileTermList 
				= gTermGenerator.calculate(nonMobileFileNameSet, dirNonMobileTerms
												, false);
		
		// Calculate Top Ten From Mobile Terms
		//System.out.println("Mobile Calculation : ");
		topRankBasedOn(true, mobileTermList, nonMobileTermList);
		//System.out.println("Non-Mobile Calculation : ");
		topRankBasedOn(false, mobileTermList, nonMobileTermList);
		
		// Grab the top ranking as feature set

		//System.out.println("Mobile Ranks : ");
		featureSet.addAll(getTermsFrom(mobileHeap));
		//System.out.println("Non-Mobile Ranks : ");
		featureSet.addAll(getTermsFrom(nonMobileHeap));
		
		int i = 1;
		System.out.println("--mobile :");
		for (String feature: getTermsFrom(mobileHeap)) {
			System.out.println(i + ". " +feature);
			i++;
		}
		System.out.println("\n--non :");
		i = 1;
		for (String feature: getTermsFrom(nonMobileHeap)) {
			System.out.println(i + ". " +feature);
			i++;
		}
		
		return featureSet;
	}
	
	@SuppressWarnings("rawtypes")
	private Set<String> getTermsFrom (MinHeap heap) {
		Set<String> featureSet = new HashSet<String>();
		for (Comparable mC : heap.get()) {
			if (mC instanceof TermScore) {
				TermScore mTermScore = (TermScore)mC;
				featureSet.add(mTermScore.getTerm());
			}
		}
		//System.out.println(featureSet);
		return featureSet;
	}
	
	private void topRankBasedOn(boolean fromMobileTermList, GroupTermList mobileTermList
														, GroupTermList nonMobileTermList) 
	{
		TermScore baseTermScore;
		Set<String> terms;
		GroupTermList baseTermList, otherTermList;
		MinHeap baseHeap;
		
		if (fromMobileTermList) {
			baseHeap = mobileHeap;
			baseTermList = mobileTermList;
			otherTermList = nonMobileTermList;
		}
		else {
			baseHeap = nonMobileHeap;
			baseTermList = nonMobileTermList;
			otherTermList = mobileTermList;
		}
		
		terms = baseTermList.getKeySet();
		for (String mTerm: terms) {
			
			Double score =  baseTermList.get(mTerm)/baseTermList.getDocumentLength();
			Double nmScore = otherTermList.get(mTerm);
			if (nmScore != null){
				score -= nmScore/otherTermList.getDocumentLength();
			}
			//System.out.println(mTerm + ":" + score);
			baseTermScore = new TermScore(mTerm, score);
			baseHeap.insert(baseTermScore);
		}
	}
	
	public static void main(String[] args) throws IOException {
		FeatureSetGenerator featureGenerator 
					= new FeatureSetGenerator.Builder().setDirMobile(FileUtil.DEF_DIR_MOBILE)
												.setDirMobileTerms(FileUtil.DEF_DIR_MOBILE_TERMS)
												.setDirNonMobile(FileUtil.DEF_DIR_NON_MOBILE)
												.setDirNonMobileTerms(FileUtil.DEF_DIR_NON_MOBILE_TERMS)
												.setFileStopWords(FileUtil.DEF_FILE_STOP_WORDS)
												.create();
		
		Set<String> featureSet = featureGenerator.generateFeatureSet();
		int i = 1;
		for (String feature: featureSet) {
			System.out.println(i + ". " +feature);
			i++;
		}
	}

}
