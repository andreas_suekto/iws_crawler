package iws_ass2.crawler;

//import crawlercommons.fetcher.
import java.util.HashMap;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.net.URL;

public class RobotChecker {

	private HashSet<String> disallowed = new HashSet<String>();
	public static HashMap<String,Boolean> noindex = 
			new HashMap<String,Boolean>();
	public static HashMap<String,Boolean> nofollow = 
			new HashMap<String,Boolean>();
	
	
//CONSTRUCTOR
	public RobotChecker(String domain){
		try{
			URL url = new URL(domain + "/" + "robots.txt");
			BufferedReader br = new BufferedReader
					(new InputStreamReader(url.openStream()));
			Pattern p = Pattern.compile("^user-agent.*\\*\\s*",
					Pattern.CASE_INSENSITIVE );
			Pattern p1 = Pattern.compile("^disallow\\s*:\\s*(.*)",
					Pattern.CASE_INSENSITIVE );
			Pattern p2 = Pattern.compile("^user-agent.*",
					Pattern.CASE_INSENSITIVE );
			Matcher m,m1,m2; 
			boolean done = false;
			
			String linein = br.readLine();
			while(linein != null && !done){
				//find user agent *
				m = p.matcher(linein);
				if(m.matches()){
					linein = br.readLine();
					//reading everything below this user agent
					while(linein != null && !done){
						//find disallows
						m1 = p1.matcher(linein);
						if(m1.matches()){
							disallowed.add(domain + m1.group(1));
						}
						linein=br.readLine();
						//if a new user-agent, we're done
						if(linein!=null){
							m2=p2.matcher(linein);
							if(m2.matches())done = true;
						}
					}
				}
				linein = br.readLine();
			}
		}catch (IOException e){
			//no robots.txt - nothing to disallow
		}
	}
	
//STATIC HELPER METHODS
	private static String getMetaTag(String url){
		/*gets robots metatag from url - if there is one. If no
		  metatag found, returns null*/
		String tag = null;
		try{
			URL page = new URL(url);
			BufferedReader br = new BufferedReader
					(new InputStreamReader(page.openStream()));
			Pattern p = 
				Pattern.compile("\\s*(<meta.*name\\s*=\\s*\"robots\".*>)\\s*",
					Pattern.CASE_INSENSITIVE );
			Pattern p1 = Pattern.compile("\\s*<\\s*/head\\s*>.*",
					Pattern.CASE_INSENSITIVE );
			Matcher m,m1; 
			
			String linein = br.readLine();
			//only search up to end of head - </head>
			m1 = p1.matcher(linein);
			while(linein != null && !m1.matches()){
				//look for robots metatag
				m = p.matcher(linein);
				if(m.matches()){
					tag = m.group(1);
				}
				linein = br.readLine();
			}
		}catch (IOException e){
			//couldn't read file
		}
		return tag;
	}
	
	private static String parseMetaTag(String tag){
		//get content
		String content = null;
		Matcher m;
		Pattern p = Pattern.compile(".*content\\s*=\\s*\"(.*)\".*",
				Pattern.CASE_INSENSITIVE );
		m=p.matcher(tag);
		if(m.matches()){
			content = m.group(1);
		}
		return content;
	}
	
	private static void createHashMaps(String url){
		noindex.put(url, false);
		nofollow.put(url,false);
		String tag = getMetaTag(url);
		//no robots metatag
		if(tag == null)	return;

		String content = parseMetaTag(tag);
		//content not found
		if(content == null)return;
	
		//content found - so set hashmaps accordingly
		if(content.toLowerCase().contains("noindex")){
			noindex.put(url, true);
		}
		if(content.toLowerCase().contains("nofollow")){
			nofollow.put(url, true);
		}
		if(content.toLowerCase().contains("none")){
			noindex.put(url, true);
			nofollow.put(url, true);
		}
	}
	
//BEHAVIOURS
	public static boolean isFollowable(String url){
		/*check nofollow hashmap. if url is listed, it's not
		  followable so return false, else return true. */
		if(!nofollow.containsKey(url)){
			createHashMaps(url);
		}
		return !nofollow.get(url);
	}
	public static boolean isIndexable(String url){
		if(!noindex.containsKey(url)){
			createHashMaps(url);
		}
		return !noindex.get(url);
	}
	
	public boolean isAllowed(String url){
		if(url.trim().length()==0){
			return false;
		}
		return !disallowed.contains(url);
	}
	
}
