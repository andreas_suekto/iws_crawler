package iws_ass2.crawler;

import iws_ass2.util.Configuration;
import iws_ass2.util.FileUtil;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.ArrayList;
import java.net.URL;
import java.net.MalformedURLException;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class Crawler {
	
	private final static int DOMAIN_POS = 7;
	private final static int MILLISECONDS = 1000;
	private final static int POLITENESS_DEFAULT = 10;
	private final static int MAXPAGES_DEFAULT = 20;
	private final static String DIR_DEFAULT = ".";
	private final static String USAGE_STRING = "./crawl [-politeness <seconds>]"
			+ " [-maxpages <pages>] [-outdir <directory>] [-restrict] seed_url";
			
	private int maxPages;	//max downloads
	private int politeness;	//seconds
	private String seedUrl; //starting URL
	private String seedDomain;	//extracted programmatically
	private String outDir;	//where to store downloaded pages
	private boolean flag;	//if true, restrict to seed descendants
	private String pagesFilename; //inlinks.txt by default
	
	//openlist = unseen pages marked for download
	LinkedList<Page> openList = new LinkedList<Page>();
	//closedlist = pages downloaded
	HashMap<String,Page> closedList = new HashMap<String,Page>();
	//seenlist = all pages seen
	HashMap<String,Page> seenList = new HashMap<String,Page>();
	//Robot checker initialised by constructor
	RobotChecker rc;
	
//CONSTRUCTORS
	public Crawler(String seedUrl) throws CrawlerException{
		this(POLITENESS_DEFAULT,MAXPAGES_DEFAULT, seedUrl);
	}
	
	public Crawler(int politeness, int maxPages, String seedUrl) 
			throws CrawlerException{
		this(politeness,maxPages,seedUrl,false);
	}
	
	public Crawler(int politeness, int maxPages, String seedUrl, boolean flag)
			throws CrawlerException{
		this(politeness,maxPages,seedUrl,flag,DIR_DEFAULT);
	}
	
	public Crawler(int politeness, int max, String seedUrl, boolean flag, 
			String outDir) throws CrawlerException{
		
		this.maxPages= max>0 ? max : MAXPAGES_DEFAULT;
		this.politeness = politeness>0 ? politeness : POLITENESS_DEFAULT;

		if(seedUrl.length()==0 || (!LinkParser.isValid(seedUrl))){
			throw new CrawlerException
				("Crawler requires a valid seed URL. Terminating program...");
		}
		this.seedUrl = seedUrl;
		
		seedDomain = extractDomain(this.seedUrl);
		this.flag = flag;
		this.outDir = outDir;
		//make sure outDir exists
		checkDir();	
		
		//robotchecker assembles disallowed list from robots.txt
		rc = new RobotChecker(seedDomain);
		if(rc.isAllowed(seedUrl) && RobotChecker.isIndexable(seedUrl)){
		//initialise openlist with seed
			Page pagePointer = new Page(seedUrl);
			openList.add(pagePointer);
			seenList.put(seedUrl,pagePointer);
		}else{
			throw new CrawlerException
			("We do not have permission to index these pages. " +
					"Terminating program...");
		}
		
		Configuration config;
		try {
			config = new Configuration(FileUtil.DEF_CONFIG_LOCATION);
		} catch (IOException e) {
			throw new CrawlerException("Failed to access config file. " +
					"Terminating program...");
		}
		pagesFilename = config.get(Configuration.FILE_PAGELIST);
		
	}
	
	public String getPagesFilename(){
		return pagesFilename;
	}

//PRIVATE HELPER METHODS
	
	private void checkDir(){
		//if outDir doesn't exist, create it
		File dir = new File(outDir);
		if (!dir.exists()) {
			dir.mkdir();
		}
	}
	
	private String extractDomain(String url){
		/*accepts normalised url. 
		  substrings and returns everything before first slash*/
		int index = DOMAIN_POS + url.substring(DOMAIN_POS).indexOf('/');
		return url.substring(0,index);
		
	}
	
	private void printPages(){
		/*writes downloaded page info to inlinks.txt so data is available 
		  to indexer. */
		ArrayList<Page> pages = new ArrayList<Page>(closedList.values());
		Collections.sort(pages);
		try{
			BufferedWriter bw = new BufferedWriter
					(new FileWriter(new File(outDir,pagesFilename)));
			for(Page each : pages){
				bw.write(each.toString()+"\n");
			}
			bw.close();
		}catch(IOException e){
			System.out.println("Unable to create "+pagesFilename);
		}
	}
	
//*****************************************************************************
//PUBLIC BEHAVIOURS
	public int crawlPages(int max){
		/*overloaded to simplify calling. Updates maxPages
		  and calls crawlPages(). Returns number of pages
		  actually downloaded. */
		this.maxPages = max;
		int returnVal = crawlPages();
		return returnVal;
	}
	
	public int crawlPages(){
		/* crawl until maxPages eligible pages have been downloaded
		   openList has been initialised with seedUrl by constructor.
		   Returns number of pages downloaded - may be less than requested
		   if openlist becomes empty.*/
		int numPages = 0;
		Page currentPage;
		String currentUrl, linein;
		ArrayList<Page> parsedLinks = new ArrayList<Page>();
		URL url;
		BufferedReader br = null;
		BufferedWriter bw = null;
		
		while(openList.size()>0 && numPages < maxPages){
			//pop link - NOTE only unseen urls added to openlist
			//so no need to check here
			currentPage = openList.removeFirst();
			currentUrl = currentPage.getUrl();
			//if disallowed, skip this url
			if(!rc.isAllowed(currentUrl) || !RobotChecker.isIndexable(currentUrl)){
				continue;
			}
			if(closedList.containsKey(currentUrl)){
				closedList.get(currentUrl).addLink(currentPage.getLink());
			}else {
				try{
					//read from url and write to file
					url = new URL(currentUrl);
					br = new BufferedReader(new InputStreamReader(url.openStream()));
					bw = new BufferedWriter
							(new FileWriter(new File(outDir,currentPage.makeName())));
	
					linein = br.readLine();
					while(linein != null){
						bw.write(linein);
						bw.newLine();
						linein = br.readLine();
					}
	
					br.close();
					bw.close();
					
					//add to closedlist
					closedList.put(currentUrl,currentPage);
					//increment page counter
					numPages++;
					
					//if page is marked as 'nofollow', don't parse it
					if(!RobotChecker.isFollowable(currentUrl))continue;
					
					//parsedLinks scrapes currentPage for links - normalises and returns 
					//them as ArrayList of Page objects
					parsedLinks = LinkParser.parsePage(outDir, 
							currentPage.makeName(currentPage.getId()),
							currentUrl, seedDomain);
					//add eligible unseen pages to openlist
					for(Page link : parsedLinks){
						currentUrl = link.getUrl();
						//only bother with urls we can use
						if((!flag && currentUrl.startsWith(seedDomain))||
							(flag && currentUrl.startsWith(seedUrl))){
							//if already seen, add link to existing page object but don't
							//add to openList
							if(seenList.containsKey(currentUrl)){
								seenList.get(currentUrl).addLink(link.getLink());
								continue;
							}else{
								openList.add(link);
								seenList.put(currentUrl, link);
							}
						}
					}
					
				}catch(MalformedURLException e){
					//skip this URL
				}catch(IOException e){
					//skip this URL
				}
				
				/*sleep requires own try-catch block*/
				try{
					Thread.sleep(politeness * MILLISECONDS);
				}catch(InterruptedException e){
					e.printStackTrace();
				}
			}
		}
		
		return numPages;
	}
	
	public void setPageId(int id){
		/*calls static method on Page to set next page id*/
		Page.setId(id);
	}
	
	public void setMaxPages(int max){
		this.maxPages = max;
	}
	
	public Page getLastPage(){
		/*Returns most recently downloaded page*/
		ArrayList<Page> pages = new ArrayList<Page>(closedList.values());
		Collections.sort(pages);
		
		return pages.get(pages.size()-1);
	}
	
//MAIN PROGRAM
	public static void main(String[] args){
		//main - called for part 1
		String nextArg, seed="";
		String out = DIR_DEFAULT;
		int max = MAXPAGES_DEFAULT, polite = POLITENESS_DEFAULT;
		boolean restrict = false;
		for(int a=0;a<args.length;a++){
			nextArg = args[a];
			if(nextArg.equals("-politeness")){
				try{
					polite = Integer.parseInt(args[a+1]);
				}catch(NumberFormatException e){
					System.out.println("Illegal argument.");
					System.out.println("Usage:" + USAGE_STRING);
					System.exit(1);
				}
				a++;
			}else if(nextArg.equals("-maxpages")){
				try{
					max = Integer.parseInt(args[a+1]);
				}catch(NumberFormatException e){
					System.out.println("Illegal argument.");
					System.out.println("Usage:" + USAGE_STRING);
					System.exit(1);
				}
				a++;
			}else if(nextArg.equals("-outdir")){
				out = args[a+1];
				a++;
			}else if(nextArg.equals("-restrict")){
				restrict = true;
			}else if(nextArg.startsWith("-")){
				System.out.println("Unknown argument: " + nextArg);
				System.out.println(USAGE_STRING);
				System.exit(1);
			}else{
				seed = nextArg;
			}
		}
		try{
			Crawler c = new Crawler(polite,max,seed,restrict,out);
			System.out.println("Searching for " + max + " page/s...");
			int pagecount = c.crawlPages();
			c.printPages();
			System.out.println(pagecount + " page/s downloaded. See " + 
					c.getPagesFilename() + ".");
		}catch(CrawlerException e){
			System.out.println(e);
			System.exit(1);
		}
		
	}
}
