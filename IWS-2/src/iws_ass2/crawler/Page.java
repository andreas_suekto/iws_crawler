package iws_ass2.crawler;

import java.util.HashMap;

public class Page implements Comparable<Page>{
	private int id = 0;
	private String url;
	private String firstLink;
	private HashMap<String, Integer> inLinks = new HashMap<String,Integer>();
	
	private static int nextId = 1;

	/* id controls filenaming: this function allows calling prog to continue
	   file naming from where it left off.*/
	public static void setId(int id){
		nextId = id;
	}
	
//CONSTRUCTOR
	public Page(String url){
		//id = nextId++;
		this.url = url;
	}
	
	public Page(String url, String inLink){
		//id = nextId++;
		this.url = url;
		inLinks.put(inLink, 1);	//first link
		firstLink = inLink;
	}

	
//GETTERS & SETTERS
	
	public int getId(){
		return id;
	}
	
	public String getUrl(){
		return url;
	}
	
	public HashMap<String,Integer> getLinks(){
		return inLinks;
	}
	
	//No setter for id or firstLink- can only be set programmatically
	
	public void setUrl(String url){
		this.url = url;
	}
	
	public void addLink(String link){
		//Adds link to Hashmap. If already exists, increments counter.
		if(inLinks.containsKey(link)){
			int value = inLinks.get(link)+1;
			inLinks.put(link,value);
		}else{
			inLinks.put(link, 1);
		}
	}
	
	public String getLink(){
		//returns one link only
		return firstLink;
	}
	
//BEHAVIOURS
	public String makeName(int filenumber){
		return String.format("%04d",  filenumber);
		
	}
	
	public String makeName(){
		//set page id and increment nextId before saving
		id = nextId++;
		return String.format("%04d",  id);
	}
	
	@Override
	public String toString(){
		/* Outputs page in format required for inlinks.txt: 
		   <id>,<url>,"<link1>":<no_links>;"<link2>":<no_links> */
		String pageString = makeName(id) + "," + url;
		if(inLinks.size()>0){
			pageString += ",";
			for(String link : inLinks.keySet()){
				//Don't output empty link
				if(link.length()>0){
					pageString += "\"" + link + "\":" + inLinks.get(link) + ";";
				}
			}
			//remove final semicolon
			pageString = pageString.substring(0,pageString.length()-1);
		}
		return pageString;
	}
	
	@Override
	public boolean equals(Object other) {
	    if (!(other instanceof Page)) {
	        return false;
	    }
	    
	    Page otherPage = (Page)other;
	    System.out.println(this);
	    System.out.println(otherPage);
	    
	    return url.equals(otherPage.url);
	   
	}
	
	public boolean equals(String pageName){
		return url.equals(pageName);
	}
	
	@Override
	public int compareTo(Page other){
		/*enables sorting by id before printing to inlinks.txt*/
		if(this.id == other.id) return 0;
		return (this.id > other.id) ? 1:-1;
	}
	
	
}