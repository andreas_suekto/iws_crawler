package iws_ass2.crawler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LinkParser {
	
	private final static int HTTP_POS = 8;
	private final static String[] FILE_TYPES = 
			new String[] {"html","htm","php","asp","aspx","jsp"};
	private final static HashSet<String> FILETYPE_SET = 
			new HashSet<String>(Arrays.asList(FILE_TYPES));
	
	
	private static String checkExtension(String url){
		/*Only allow listed filetypes. If not allowed
		  returns empty string*/
		String extension;
		url = url.trim();
		int index = url.lastIndexOf(".");
		if(index > 0){
			extension = url.substring(index+1);
			if(!FILETYPE_SET.contains(extension)){
				url = "";
			}
		}
		//if index = 0, url = default type
		return url;
	}

	private static String cleanUrl(String url,String parentUrl,String domain){
		/* Normalises url - handles # / .. . or filename only.
		   Validates using URL. If url is invalid or doesn't end with
		   recognised extension, returns an empty string */
		int slashPos;
		url = checkExtension(url);
		if(url.length() == 0 || url.startsWith("#")){
			return "";
		}else if(url.startsWith("/")){
			url = domain + url;
		}else if(url.startsWith("..")){
			slashPos = parentUrl.lastIndexOf("/");
			if(slashPos>HTTP_POS){
				url = parentUrl.substring(0,slashPos)+url.substring(2);
			}
		}else if(url.startsWith(".")){
			url = parentUrl + url.substring(1);
		}else if(!url.startsWith("http://") && !url.startsWith("https://")){
			slashPos = parentUrl.lastIndexOf("/");
			if(slashPos>HTTP_POS){
				url = parentUrl.substring(0,slashPos+1)+url;
			}
		}
		
		if(!isValid(url))url="";
		
		return url;
		
	}
	
	public static boolean isValid(String url){
		//if String can passes both tests, it's valid
		URL validator;
		boolean valid = false;
		try{
			validator = new URL(url);
	        validator.toURI();
	        valid = true;
        }catch(MalformedURLException e) {
            url = "";
	    }catch(URISyntaxException e) {
	        url = "";
		}
		return valid;
	}
	
	
	public static ArrayList<Page> parsePage(String dir, String filename, 
			String pageUrl, String domain){
		/*Parses page passed in as filename. For each link, if from seed domain
		  creates Page object. Returns ArrayList of parsed links. */
		ArrayList<Page> parsedLinks = new ArrayList<Page>();
		String linein, currentUrl, currentLink, longline, nextline;
		//pattern extracts url and anchor text from a tag
		Pattern p = Pattern.compile(".*<a\\s.*");
		Pattern p1 = Pattern.compile(".*</a>.*");
		Pattern p2 = Pattern.compile
				(".*<a.*href\\s*=\\s*\"(\\S*)\".*>(.*)</a>.*");
		Matcher m, m1, m2;
		try{
			BufferedReader br = new BufferedReader
					(new FileReader(new File(dir,filename)));
			linein = br.readLine();
			while(linein!=null){
				//handle anchor tags that extend over multiple lines
				m = p.matcher(linein);
				if(m.matches()){
					
					//found <a 
					longline = linein;
					m1 = p1.matcher(linein);
					while(!m1.matches()){
						nextline = br.readLine();
						longline += nextline;
						if(nextline !=  null){
							m1 = p1.matcher(nextline);
						}
					}
					//found </a> - got whole link.
					m2 = p2.matcher(longline);
					if(m2.matches()){
						currentUrl = cleanUrl(m2.group(1).trim(),pageUrl, domain);
						if(currentUrl==""){
							linein = br.readLine();
							continue;	//if link is bad, don't save it
						}
						currentLink = m2.group(2).trim();
						parsedLinks.add(new Page(currentUrl,currentLink));
					}
				}
				linein = br.readLine();
			}
			br.close();
		}catch(Exception e){
			System.out.println("Unable to locate file: " + filename);
		}
		return parsedLinks;
	}
	
}
