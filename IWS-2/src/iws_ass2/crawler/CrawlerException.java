package iws_ass2.crawler;

public class CrawlerException extends Exception {

	//required to extend serialised type
	private static final long serialVersionUID = 1;
	
	CrawlerException(String msg){
		super(msg);
	}

}
