package iws_ass2.util;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

public class FileUtil {
	public final static String DEF_CONFIG_LOCATION = "config.properties";
	public final static String DEF_FILE_STOP_WORDS = "res/english.stop.txt";
	public final static String DEF_FILE_INLINKS = "res/inlinks.txt";
	public final static String DEF_MODEL_FILE = "res/model.arff";
	public final static String DEF_MODEL_INLINKS_FILE = "res/model-inlinks.arff";
	
	public final static String DEF_DIR_MOBILE = "res/mobile-page/";
	public final static String DEF_DIR_NON_MOBILE = "res/nonmobile-page/";
	public final static String DEF_DIR_MOBILE_TERMS = "res/mobile-page/terms/";
	public final static String DEF_DIR_NON_MOBILE_TERMS = "res/nonmobile-page/terms/";

//	public final static String DEF_DIR_MOBILE = "res/mobile-page/testdata";
//	public final static String DEF_DIR_NON_MOBILE = "res/nonmobile-page/testdata";
//	public final static String DEF_DIR_MOBILE_TERMS = "res/mobile-page/testdata/terms/";
//	public final static String DEF_DIR_NON_MOBILE_TERMS = "res/nonmobile-page/testdata/terms/";

	
	public static String getFormattedScore (double score) {
		return String.format("%.4f", score);
	}
	
	public static String getTermsFileName (int id) {
		return getFileName(id) + getTermsFileExtension();
	}
	
	public static String getTermsFileExtension() {
		return ".terms";
	}
	
	public static String getFileName(int id) {
		return String.format("%04d", id);
	}
	
	/**
	 * Return the set of file name under a directory.
	 * File returned are those that is in accordance
	 * to the getFileName syntax.
	 * @param directoryName
	 * @param startingId
	 * @return
	 */
	public static Set<String> getFileNameSet(String directoryName, int maxLimit) {
		File directory = new File(directoryName);
		if (directory.exists() == false)
			return null;
		
		// clean directory name
		directoryName = FileUtil.normaliseDir(directoryName)+ File.separator;
		
		String[] filenames = directory.list();
		if (maxLimit == -1) {
			// grab all of it
			maxLimit = filenames.length;
		}
		Set<String> fileNameSet = new HashSet<String>();
		for (int i = 0; i < maxLimit; i++) {
			String filename = filenames[i];
			filename = directoryName + filename;
			File file = new File(filename);
			if (file.exists() && file.isFile())
				fileNameSet.add(filename);
		}
		
		return fileNameSet;
	}
	
	public static Set<String> getFileNameSet(String directoryName) {
		return getFileNameSet(directoryName, -1);
	}
	
	/**
	 * Making sure to return a directory name
	 * based on File IO absolute path.
	 * @param dir
	 * @return
	 */
	public static String normaliseDir(String dir) {
		File dirFile = new File(dir);
		
		dir = dirFile.getAbsolutePath();
		
		return dir;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println(getFileName(1));
	}

}
