package iws_ass2.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Configuration {
	// Constant Key for accessing configuration value
	public final static String NUM_MOBILE_FEATURE = "mobileFeatureNumber";
	public final static String NUM_NONMOBILE_FEATURE="nonmobileFeatureNumber";
	
	public final static String FILE_STOP_WORDS = "stopwordsFile";
	public final static String FILE_INLINKS = "inlinksFile";
	public final static String FILE_MODEL = "modelFile";
	public final static String FILE_PAGELIST = "pagesFile";
	
	public final static String DIR_SAVED_PAGES = "crawlDir";
	public final static String DIR_MOBILE = "mobileDir";
	public final static String DIR_NON_MOBILE = "nonmobileDir";
	public final static String DIR_MOBILE_TERMS = "mobileTermsDir";
	public final static String DIR_NON_MOBILE_TERMS = "nonmobileTermsDir";
	
	private Properties configuration = new Properties();
	
	public Configuration (String configFileLocation) throws IOException {
		InputStream configFile = new FileInputStream(configFileLocation);
		configuration.load(configFile);
	}
	
	public String get(String key) {
		return configuration.getProperty(key);
	}
}
